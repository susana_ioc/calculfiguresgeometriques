package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author susangn
 */
public class Triangle implements FiguraGeometrica {

    private final double costat1;
    private final double costat2;
    private final double costat3;

    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del costat 1 del triangle: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu la longitud del costat 2 del triangle: ");
        this.costat2 = scanner.nextDouble();
        System.out.println("Introduïu la longitud del costat 3 del triangle: ");
        this.costat3 = scanner.nextDouble();
    }

    @Override
    public double calcularArea() {
        // Calcula el semiperímetre
        double s = (costat1 + costat2 + costat3) / 2;

        // Calcula l'área fent us de la fórmula de Herón
        double area = Math.sqrt(s * (s - costat1) * (s - costat2) * (s - costat3));

        return area;
    }

    @Override
    public double calcularPerimetre() {
        return costat1 + costat2 + costat3;
    }
}
